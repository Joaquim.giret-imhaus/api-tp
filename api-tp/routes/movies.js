const express = require('express');
// Lodash utils library
const _ = require('lodash');
const axios = require('axios');

const router = express.Router();

// Create RAW data array
let movies = [{
  id: 0,
  movie: "Star Wars: Episode IV - A New Hope",
  yearOfRelease: "1977",
  duration: "121 min",
  actors: "Mark Hamill, Harrison Ford, Carrie Fisher, Peter Cushing",
  poster: "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
  boxOffice: "N/A",
  rottenTomatoesScore: "93%"
 }
];

const apiKey = "ae52fd9b";

/* GET movies listing. */
router.get('/', (req, res) => {
  // Get List of movies and return JSON
  res.status(200).json({ movies });
});

/* GET one movie. */
router.get('/:id', (req, res) => {
  const { id } = req.params;
  // Find movie in DB
  const movie = _.find(movies, ["id", id]);
  // Return movie
  res.status(200).json({
    message: 'Film found!',
    movie 
  });
});

/* PUT new movie. */
router.put('/', (req, res) => {
  // Get the data from request from request
  const { movie } = req.body;
  // Create new unique id
  const id = _.uniqueId();

  axios.get(`http://www.omdbapi.com/?apiKey=${apiKey}&t=${movie}`)
        .then((response) => {
          console.log(response.data);
          // Insert it in array
          movies.push({ movie,
                        id,
                        yearOfRelease: response.data.Released,
                        duration: response.data.Runtime,
                        actors: response.data.Actors,
                        poster: response.data.Poster,
                        boxOffice: response.data.BoxOffice,
                        rottenTomatoesScore: response.data.Ratings[1].Value
                      });
        })

  // Return message
  res.json({
    message: `Just added ${id}`,
    movie: { movie, id }
  });
});

/* DELETE movie. */
router.delete('/:id', (req, res) => {
  // Get the :id of the movie we want to delete from the params of the request
  const { id } = req.params;

  // Remove from "DB"
  _.remove(movies, ["id", id]);

  // Return message
  res.json({
    message: `Just removed ${id}`
  });
});

/* UPDATE user. */
router.post('/:id', (req, res) => {
  // Get the :id of the movie we want to update from the params of the request
  const { id } = req.params;
  // Get the new data of the movie we want to update from the body of the request
  const { movie } = req.body;
  // Find in DB
  const movieToUpdate = _.find(movies, ["id", id]);

  axios.get(`http://www.omdbapi.com/?apiKey=${apiKey}&t=${movieToUpdate}`)
        .then((response) => {
          console.log(response.data);
          // Insert it in array
          movieToUpdate ={ movie,
                        id,
                        yearOfRelease: response.data.Released,
                        duration: response.data.Runtime,
                        actors: response.data.Actors,
                        poster: response.data.Poster,
                        boxOffice: response.data.BoxOffice,
                        rottenTomatoesScore: response.data.Ratings[1].Value
                      };
        })
  // Update data with new data (js is by address)
  movieToUpdate.movie = movie;

  // Return message
  res.json({
    message: `Just updated ${id} with ${movie}`
  });
});

module.exports = router;
